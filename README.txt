Shopzilla PPC integration
Version: 	1.1
Author:		Parameshwar Babu
e-Mail:		info@paramprojects.com / netminster@gmail.com
Website:	http://paramprojects.com


Compatability:
Drupal 		6.1
Ubercart 	2.x
XML:		1.0


------------------------------------------------------------------------------------------------------
QUICK INFO
==========
To access the shopzilla data feed, you just need to access: http://www.yoursite.com/products/shopzilla.txt

PURPOSE
=======
Shopzilla is a leading shopping site popular across the globe. Shopzilla is a powerful method of PPC advertising for retailers. If you are running an ecommerce/shopping site, you will be tempted to publish your products to shopzilla store. However, it takes a lot of time to find get proper documentation from shopzilla staff for publishing/uploading your products. We have undergone this process and hence I hope this module help a lot of people in the drupal community. Once your products are published in shopzilla website, you would be able to generate a lot of sales as there are large number of affiliates for shopzilla.


STEP BY STEP QUIDE
==================

Assuming you have set up ubercart and created products, follow the instructions below.

1) Upload the module into sites/all/modules folder of your drupal installation.
2) Enable the module. Go to admin/store/settings/shopzilla and you may configure
the title and description for your product feed.
3) Now, you can access your feed from here http://www.yoursite.com/products/shopzilla.txt
and then submit it to your shopzilla store.
   


Contact
-------
I and my team handle a lot of ecommerce/drupal projects from clients around the world - so feel free to contact me
if you need any help as we have enough resources to handle plenty of work.

EMail:		info@paramprojects.com / netminster@gmail.com
Website:	http://paramprojects.com/website

